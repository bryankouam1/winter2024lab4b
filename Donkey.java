public class Donkey{
	private int lifespan;
	private String bestFriend;
	private String generateSound;
	
	public Donkey(int lifespan,String bestFriend,String generateSound){
		this.lifespan = lifespan;
		this.bestFriend=bestFriend;
		this.generateSound=generateSound;
		
	}
	
	

	
	//get method for variable lifespan
	public int getLifeSpan(){
		return this.lifespan;
	}
	

	
	//get method for variable bestFriend
	public String getBestFriend(){
		return this.bestFriend;
	}
	
	//set method for variable generateSound
	public void setGenerateSound(String newGenerateSound){
		this.generateSound=newGenerateSound;
		
	}
	
	//get method for variable generateSound
	public String getGenerateSound(){
		return this.generateSound;
	}
	

	
	
	
	public void LiveOrDie(){
		if(this.lifespan > 60){
			System.out.println("Rest In Peace Donkey");
		}
		else{
			System.out.println("Enjoy Life ;)");
		}
	}
	public void FriendShip(){
		if(this.bestFriend.equals("shrek") || generateSound.equals("hey")){
			System.out.println("Happy Donkey");
		}
		else{
			System.out.println("Aye where Shrek at ?!");
		}
	}
}