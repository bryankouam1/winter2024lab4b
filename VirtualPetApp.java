import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		
		Donkey[] drove = new Donkey[1];
		
		for(int i=0; i < drove.length;i++){
			
			
			System.out.println("Enter Donkeys age");
			int lifespan=Integer.parseInt(reader.nextLine());
			
			System.out.println("Enter who is Donkeys best friend");
			String name=reader.nextLine();
			
			System.out.println("Enter a greeting");
			String generateSound= reader.nextLine();
			
			drove[i] = new Donkey(lifespan,name,generateSound);
		
		}
		System.out.println("BEFORE VALUES: ");
		System.out.println("lifespan: " + drove[drove.length-1].getLifeSpan());
		System.out.println("Bestfriend: " + drove[drove.length-1].getBestFriend());
		System.out.println("Sound: " + drove[drove.length-1].getGenerateSound());
	
		System.out.println("Enter a new greeting");
		String changedValue = reader.nextLine();
		drove[drove.length-1].setGenerateSound(changedValue);
		
		System.out.println("AFTER VALUES: ");
		System.out.println("lifespan: " + drove[drove.length-1].getLifeSpan());
		System.out.println("Bestfriend: " + drove[drove.length-1].getBestFriend());
		System.out.println("Sound: " + drove[drove.length-1].getGenerateSound());
		
		
	}
}
		